<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Connecta AG Dev Team <typo3@connecta.ag>, Connecta AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace CAG\DynamicData\Services;

use CAG\DynamicData\DynamicDataProcessor\DataProcessor;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class DataService
{
    const TYPE_MANUAL_QUERY = 0;

    protected $objectManager = null;

    /**
     * Holds the setup of dynamic typoscript to merge
     * @var array
     */
    protected $constants = [];

    protected $configuration = [];

    /**
     * @param $theRootLine
     * @param $pObj
     */
    public function process(&$theRootLine, &$pObj)
    {
        $this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        // load configuration
        $configuration = $this->loadTSFile('EXT:mhr/Configuration/TypoScript/Plugins/dynamic.ts');

        /** @var \TYPO3\CMS\Core\TypoScript\TypoScriptService $typoscriptService */
        $typoscriptService = $this->objectManager->get(\TYPO3\CMS\Core\TypoScript\TypoScriptService::class);
        $this->configuration = $typoscriptService->convertTypoScriptArrayToPlainArray($configuration);

        // iterate dynamic configurations
        foreach ($this->configuration['dynamicElements'] as $configurationItem) {
            $items = $this->processDynamicConfig(array_reverse($theRootLine), $configurationItem);

            foreach ($items as $field => $value) {
                $value = $this->processMappingOptions($configurationItem, $field, $value);
                $GLOBALS['DYNAMIC_DATA'][$this->getMappedFieldname($configurationItem, $field)] = $value;
            }
        }
    }

    /**
     * Process "dynamicElements" configuration element
     *
     * @param $theRootLine
     * @param $config
     * @return array
     */
    private function processDynamicConfig($theRootLine, $config)
    {
        $processedData = [];
        if (!empty($config['dataProcessor'])) {
            $dataProcessor = GeneralUtility::makeInstance($config['dataProcessor'], $config);
        } else {
            $dataProcessor = GeneralUtility::makeInstance(\CAG\DynamicData\DynamicDataProcessor\DataProcessor::class, $config);
        }

        if ($dataProcessor instanceof DataProcessor) {
            if (empty($config['searchRootline'])) {
                // only inspect current page
                $currentPageData = end($theRootLine);
                $data = $this->fetchDataByDataProcessor($dataProcessor, $currentPageData['uid'], md5($config['query']));

                if (!empty($data) && is_array($data)) {
                    ArrayUtility::mergeRecursiveWithOverrule($processedData, $data);
                }
            } else {
                // walk through the rootline
                foreach ($theRootLine as $page) {
                    $data = $this->fetchDataByDataProcessor($dataProcessor, $page['uid'], md5($config['query']));

                    if (!empty($data) && is_array($data)) {
                        ArrayUtility::mergeRecursiveWithOverrule($processedData, $data);
                    }
                }
            }
        }
        return $processedData;
    }

    /**
     * Load configuration file
     * @param $filename
     * @return array
     */
    private function loadTSFile($filename)
    {
        $absolutePath = GeneralUtility::getFileAbsFileName($filename);
        $content = file_get_contents($absolutePath);

        /** @var \TYPO3\CMS\Core\TypoScript\Parser\TypoScriptParser $parser */
        $parser = $this->objectManager->get(\TYPO3\CMS\Core\TypoScript\Parser\TypoScriptParser::class);
        $parser->parse($content);

        return $parser->setup;
    }

    /**
     * Get mapped fieldname from configuration file
     *
     * @param $configuration
     * @param $field
     * @return string
     */
    protected function getMappedFieldname($configuration, $field)
    {
        $configPrefix = $configuration['configPrefix'] ?? 'dynamic_data';
        $fieldName = '';
        if (!empty($configPrefix)) {
            $fieldName .= $configPrefix;
        }

        if (!array_key_exists($field, $configuration['fieldMapping'])) {
            // No mapping available? => keep current field name
            if (strpos($field, '_') !== false) {
                $fieldName .= GeneralUtility::underscoredToUpperCamelCase($field);
            } else {
                $fieldName .= ucfirst($field);
            }
        } elseif (is_array($configuration['fieldMapping'][$field]) && !empty($configuration['fieldMapping'][$field]['key'])) {
            // Mapping with further options defined => extract new key
            $fieldName .= ucfirst($configuration['fieldMapping'][$field]['key']);
        } else {
            $fieldName .= ucfirst($configuration['fieldMapping'][$field]);
        }
        return $fieldName;
    }

    /**
     * Process value with defined mapping options
     *
     * @param $configuration
     * @param $field
     * @param $value
     * @return string
     */
    protected function processMappingOptions($configuration, $field, $value)
    {
        $fieldMappingConfig = $configuration['fieldMapping'][$field];

        // mapping with some options defined. We have to process the options
        if (is_array($fieldMappingConfig)) {
            // Sanitizing strings for use in Google Tag Manager's dataLayer Javascript code
            if (!empty($fieldMappingConfig['sanitize'])) {
                $value = addslashes(str_replace("'", '', $value));
            }

            // remove line breaks
            if (!empty($fieldMappingConfig['removeLineBreaks'])) {
                $value = trim(preg_replace('/\s\s+/', ' ', $value));
            }

            // strip tags
            if (!empty($fieldMappingConfig['stripTags'])) {
                $value = str_replace('&nbsp;', '', $value);
                $value = strip_tags(trim($value));
            }
        }
        return $value;
    }

    protected function fetchDataByDataProcessor(DataProcessor $dataProcessor, $pageId, $uniqueConfigIdentifier)
    {
        $cacheIdentifier = $pageId . '-' . intval($_GET['L']) . '-' . $uniqueConfigIdentifier;
        $cache = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager')->getCache('dynamic_data');

        if (($data = $cache->get($cacheIdentifier)) === false) {
            $data = $dataProcessor->fetchData($pageId);

            if ($data === false) {
                $data = '';
            }
            // Save value in cache
            $cache->set($cacheIdentifier, $data);
        }
        return $data;
    }
}

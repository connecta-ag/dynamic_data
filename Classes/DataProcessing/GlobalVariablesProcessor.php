<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Connecta AG Dev Team <typo3@connecta.ag>, Connecta AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace CAG\DynamicData\DataProcessing;

use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * Class GlobalVariablesProcessor
 *
 * Add global variables to fluid template
 */
class GlobalVariablesProcessor implements \TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface
{
    //TODO set by configuration
    protected $prefix = 'dynamicData';

    /**
     * Process content object data
     *
     * @param ContentObjectRenderer $cObj The data of the content element or page
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        if (!empty($GLOBALS['DYNAMIC_DATA']) && is_array($GLOBALS['DYNAMIC_DATA'])) {
            foreach ($GLOBALS['DYNAMIC_DATA'] as $field => $value) {
                $fieldName = $this->prefix . ucfirst($field);
                $processedData[$fieldName] = $value;
            }
        }
        return $processedData;
    }
}

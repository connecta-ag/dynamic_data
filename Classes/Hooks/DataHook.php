<?php
namespace CAG\DynamicData\Hooks;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class DataHook extends \TYPO3\CMS\Core\TypoScript\TemplateService
{
    public function checkDataSubmission()
    {
        $dataService = GeneralUtility::makeInstance('CAG\\DynamicData\\Services\\DataService');
        $dataService->process($GLOBALS['TSFE']->rootLine, $this);
    }
}

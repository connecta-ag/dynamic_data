<?php
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Connecta AG Dev Team <typo3@connecta.ag>, Connecta AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace CAG\DynamicData\DynamicDataProcessor;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

abstract class DataProcessor
{
    protected $config = [];
    protected $cObj = null;

    /**
     * DataProcessor constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->config = $config;
        /** @var  \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer $cObj */
        $this->cObj = GeneralUtility::makeInstance(\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer::class);
    }

    /**
     * Default method to fetch data by query defined in the configuration.
     * This Method can be overridden by a custom data processor.
     *
     * @param $pageId
     * @return array
     */
    public function fetchData($pageId)
    {
        $data = [];

        if (!empty($this->config['query'])) {
            /** @var \Doctrine\DBAL\Connection $connection */
            $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionByName(ConnectionPool::DEFAULT_CONNECTION_NAME);
            $result = $connection->executeQuery($this->config['query'], ['pageId' => $pageId]);
            $data = $result->fetch();
        }
        // no query defined. You have to fetch the data manually.

        return $data;
    }

    /**
     * Method to allow record localization within the custom data processor.
     *
     * @param $row
     * @param $table
     * @param $uid
     * @param $pid
     * @param $fields
     * @param array $fieldNameMapping
     */
    protected function localizeRecord(&$row, $table, $uid, $pid, $fields, $fieldNameMapping = [])
    {
        $fieldsToLocalize = [
            'uid' => $uid,
            'pid' => $pid,
        ];
        $fieldsToLocalize = array_merge($fieldsToLocalize, $fields);
        $localizedRecord = $GLOBALS['TSFE']->sys_page->getRecordOverlay($table, $fieldsToLocalize, intval($_GET['L']));

        foreach (array_keys($fields) as $fieldName) {
            $newFieldName = $fieldName;
            if (!empty($fieldNameMapping) && array_key_exists($fieldName, $fieldNameMapping)) {
                $newFieldName = $fieldNameMapping[$fieldName];
            }

            $row[$newFieldName] = $localizedRecord[$fieldName];
        }
    }
}

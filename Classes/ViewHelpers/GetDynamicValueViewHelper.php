<?php

namespace CAG\DynamicData\ViewHelpers;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Connecta AG Dev Team <typo3@connecta.ag>, Connecta AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use FluidTYPO3\Vhs\Traits\TemplateVariableViewHelperTrait;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * ViewHelper to get global dynamic data
 */
class GetDynamicValueViewHelper extends AbstractViewHelper
{
    use TemplateVariableViewHelperTrait;

    /**
     * @param string $key
     * @return string
     */
    public function render($key)
    {
        $value = '';
        if (!empty($GLOBALS['DYNAMIC_DATA']) && array_key_exists($key, $GLOBALS['DYNAMIC_DATA'])) {
            $value = $GLOBALS['DYNAMIC_DATA'][$key];
        }

        return $this->renderChildrenWithVariableOrReturnInput($value);
    }
}

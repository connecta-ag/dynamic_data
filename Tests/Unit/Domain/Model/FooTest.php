<?php
namespace CAG\DynamicData\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class FooTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \CAG\DynamicData\Domain\Model\Foo
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \CAG\DynamicData\Domain\Model\Foo();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}

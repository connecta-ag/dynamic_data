# README #

Diese Extension dient dazu oft benötigte Informationen global zur Verfügung zu stellen, damit diese nicht in verschiedenen Plugins / Templates extra ermittelt werden müssen.

## Verwendung ##
**TypoScript**
```
1 = TEXT
1.data = GLOBAL:DYNAMIC_DATA|dynamicDataKey
```

**Alle Informationen im Fluid-Template bereitstellen**
```
10 = FLUIDTEMPLATE
10 {
    # add dynamic data as fluid variables
    dataProcessing {
       1 = CAG\DynamicData\DataProcessing\GlobalVariablesProcessor
    }
}
```

**Informationen über ViewHelper ermitteln**

```
<dynamic:getDynamicValue key="dynamicDataKey" as="dynamicDataValue">
	<f:debug>{dynamicDataValue}</f:debug>
</dynamic:getDynamicValue>

// inline
{dynamic:getDynamicValue(key: 'dynamicDataKey')}
```

## Konfiguration ##
Aussehen einer beispilhaften Konfiguration:

```
dynamicElements {
  0 {
    configPrefix = dynamicTs
    query (
      SELECT * FROM pages WHERE pid = :pageId
    )
    dataProcessor = CAG\Tmpl\DynamicDataProcessor\GlobalPageInformation
    searchRootline = 1

    fieldMapping {
      title {
        sanitize = 1
        key = pageTitle
      }
      abstract = pageDescription
    }
  }
  1 {
    ...
  }
}

```

**configPrefix (optional)**

Der Wert dieser Konfiguration wird als Prefix allen global zur Verfügung gestellten Daten-Keys angefügt.

**query (optional => dataProcessor verwenden)**

Hier kann eine manuelle Query definiert werden. Die folgenden Platzhalter sind innerhalb der Query möglich:
* :pageId = Id der zu untersuchenden Seite. Ist die Konfiguration "searchRootline" aktiv, wird die Rootline durchlaufen und die jeweilige pid an dieser Stelle eingefügt.

**dataProcessor (optional => query verwenden)**

Über einen eigenen DataProcessor ist es möglich in den Prozess der Ermittlung der Daten einzugreifen bzw. diesen an dieser Stelle zu definieren, wenn keine Query verwendet wird. Vorstellbar ist, dass an dieser Stelle eine externe Informationen wie von einer API ermittelt werden können. Weitere Informationen hierzu siehe Abschnitt "DataProcessor". 

**fieldMapping (optional)**

Zur Übersetzung der Ermittelten Daten ist es möglich ein Mapping / Umbenennung der Felder vorzunehmen. Zudem können an dieser Stelle weitere Optionen für die Felder definiert werden (z.B. sanitize).

### DataProcessor ###

Über den DataProcessor ist es möglich Daten individueller über eine eigens definierte Query zu ermitteln oder auch Informationen von anderen Quellen (API, Curl-Request) zu ermitteln und anschließend global zur Verfügung zu stellen. 
Der der verwendete DataProcessor muss den bereitgestellten abstrakten "CAG\DynamicData\DynamicDataProcessor\DataProcessor" erweitern. Darin kann anschließend die Methode "fetchData" an die eigenen bedürfnisse angepasst werden.

## To do ##
* Cachingmechanismus hinzufügen
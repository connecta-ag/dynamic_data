<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['checkDataSubmission'][] = 'CAG\DynamicData\Hooks\DataHook';

if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['dynamic_data'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['dynamic_data'] = [
        'backend' => 'TYPO3\CMS\Core\Cache\Backend\Typo3DatabaseBackend',
        'options' => [
            'defaultLifetime' => 3601
        ],
    ];
}
